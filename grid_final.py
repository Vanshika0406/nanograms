import matplotlib.pyplot as plt

def disp_grid():
	grid = [[1, 2, 3, 4, 5],
        	[6, 7, 8, 9, 10],
        	[11, 12, 13, 14, 15],
        	[16, 17, 18, 19, 20],
        	[21, 22, 23, 24, 25]]

	plt.axis('off')

	for i in range(len(grid) + 1):
    	plt.plot([i, i], [0, len(grid)], color = 'black')
    	plt.plot([0, len(grid)], [i, i], color = 'black')

	plt.show()
